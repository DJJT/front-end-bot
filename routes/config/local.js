const _ = require('lodash');

// Module variables
const config = require('./local.json');
const defaultConfig = config.development;
const environment = process.env.NODE_ENV || 'local';
const environmentConfig = config[environment];
const finalConfig = _.merge(defaultConfig, environmentConfig);

global.gConfig = finalConfig;

module.exports = finalConfig;