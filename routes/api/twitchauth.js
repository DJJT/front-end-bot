'use strict'

// Requires
const express = require('express');
const router = express.Router();
const passport = require('passport');
const handlebars = require('handlebars');
const OAuth2Strategy = require('passport-oauth').OAuth2Strategy;
const config = require('../config/local');

let client_id = config.twitchauth.twitch_client_id;

// Override passport profile function to get user profile from Twitch API //
OAuth2Strategy.prototype.userProfile = function(accessToken, done) {
    let options = {
        url: 'https://api.twitch.tv/helix/users',
        method: 'GET',
        headers: {
            'Client-ID': client_id,
            'Accept': 'application/vnd.twitchtv.v5+json',
            'Authorization': 'Bearer ' + accessToken
        }
    };

    request(options, function (error, response, body) {
        if (response && response.statusCode == 200) {
            done(null, JSON.parse(body));
        } else {
            done(JSON.parse(body));
        }
    });
}
passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(user, done) {
    done(null, user);
});
passport.use('twitch', new OAuth2Strategy({
    authorizationURL: 'https://id.twitch.tv/oauth2/authorize',
    tokenURL: 'https://id.twitch.tv/oauth2/token',
    clientID: client_id,
    clientSecret: config.twitch_secret,
    callbackURL: config.callback_url,
    state: true
},
function(accessToken, refreshToken, profile, done) {
    profile.accessToken = accessToken;
    profile.refreshToken = refreshToken;

    /** Store user profile in db
     * User.findOrCreate(..., function(err, user) {
     * done(err, user);
     * });
     */
}
));

// ROUTE for OAuth redirect
router.get('/auth/twitch', passport.authenticate('twitch', {scope:'user_read'}));
router.get('/auth/twitch/callback', passport.authenticate('twitch', {successRedirect: '/home', failureRedirect: '/'}));

// Define a simple template to safely generate HTML with values from user's profile
let template = handlebars.compile(`
<html><head><title>Twitch Auth Sample</title></head>
<table>
    <tr><th>Access Token</th><td>{{accessToken}}</td></tr>
    <tr><th>Refresh Token</th><td>{{refreshToken}}</td></tr>
    <tr><th>Display Name</th><td>{{display_name}}</td></tr>
    <tr><th>Bio</th><td>{{bio}}</td></tr>
    <tr><th>Image</th><td>{{logo}}</td></tr>
</table></html>`);

// If user has an authenticated session, display it, otherwise display link to authenticate
router.get('/home', function (req, res) {
    if(req.session && req.session.passport && req.session.passport.user) {
        res.render(template(req.session.passport.user));
    } else {
        res.render('index');
    }
    console.log('Twitch API Auth.');
});

module.exports = router;