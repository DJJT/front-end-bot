'use strict'

// Requires 
const express = require('express');
const router = express.Router();

// GET Index page
router.get('/', (req, res, next) => {
    res.render('index', { title: 'Login' });
    // debugging
    console.log('Index served.');
});

// Export
module.exports = router;