'use strict'

// Requires
const express = require('express');
const router = express.Router();

// Routes
const mongoData = require('../database/connect');
const fetchCmdData = require('./js/dataList');

// GET Home page
router.get('/home', function(req, res) {
    // variables
    let dataList = fetchCmdData(mongoData.data)
    let commandList = dataList[0];
    let messageList = dataList[1];
    res.render("home", {
        title: "Tables",
        "userData": mongoData.data,
        "commandList": commandList,
        "messageList": messageList,
        "dataList": dataList
    });
    // debugging
    console.log('Home served.');
});

// Export
module.exports = router;