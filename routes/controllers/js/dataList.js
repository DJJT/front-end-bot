'use strict'

module.exports = function(data) {
    // Variables
    let commandList = [];
    let messageList = [];
    let dataList = [];
    // Validate data exists
    if (data.length !== 0) {
        // Loop through data
        data.forEach(function(data) {
            // Pull key values from data
            Object.keys(data).forEach(key => {
                // Exclude id and container id keys
                if (key !== "_id" && key !== "container_id") {
                    // Load key array
                    commandList.push(key);
                    // Load value array
                    messageList.push(data[key]);
                }
            });
        });
        // Load key array && value array into data array
        dataList.push(commandList, messageList);
        return dataList;
    };
}