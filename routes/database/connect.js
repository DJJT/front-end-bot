'use strict'

// Requires
let MongoClient = require('mongodb').MongoClient;
const config = require('../config/local');

// Db data container
let data = [];

// Connect to mongodb
MongoClient.connect(config.uri, {useUnifiedTopology: true, useNewUrlParser: true}, function(err, db) {
    if (err) throw err;
    // Select database
    let dbo = db.db(config.database);
    // Find collection data in database
    dbo.collection(config.collection).find({}).toArray(function(err, result)
    {
        // Data parameter = empty array
        if (err) throw err;
        console.log("Successful DB Connection!");
        data = result;
        db.close();
        // Export db data
        exports.data = data;
    });
});
