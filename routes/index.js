'use strict'

// Requires
const express = require('express');
const router = express.Router();

// Route modules
const indexRouter = require('./controllers/index');
const homeRouter = require('./controllers/home');
const authRouter = require('./api/twitchauth');

// Controllers
router.use(indexRouter);
router.use(homeRouter);
router.use(authRouter);

// Export
module.exports = router;