// Requires
const express = require('express'),
    path = require('path'),
    passport = require('passport'),
    bodyParser = require('body-parser'),
    session = require('express-session');

// Environmental variable
process.env.NODE_ENV = 'local';

// Route modules
const serveRouter = require('./routes/index');
const config = require('./config/local');

// Build pipeline
const app = express();

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Configure middleware
app.use(session({secret: config.session_secret, resave: false, saveUninitialized: false}));
app.use(express.static('public'));
app.use(passport.initialize());
app.use(passport.session());

// Static stylesheet
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: true}));

// Application routes
app.use(serveRouter);

// Add listen for localhost response //
const server = app.listen(config.node_port, () => {
    console.log(`Site running -> PORT ${server.address().port}`);
});

module.exports = app;