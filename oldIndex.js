// Add NPM kits //
const express = require('express');
const passport = require('passport');
const request = require('request');
const session = require('express-session');
const handlebars = require('handlebars');
const OAuth2Strategy = require('passport-oauth').OAuth2Strategy;
const bodyParser = require('body-parser');
const { type } = require('os');
const MongoClient = require('mongodb').MongoClient;

// OAuth Constant Variables //
const TWITCH_CLIENT_ID = 'qjfxczn80q3yar3oqzva45yyo2ldyi';
const TWITCH_SECRET = '8k2veg4kq2y1qnj6hfdflso8hika5b';
const SESSION_SECRET = 'BigSuperSecret';
const CALLBACK_URL = 'http://localhost:8080/home'

// Mongo Action //
let data = []; /* Temporarily used for test render */
const uri = "mongodb+srv://Taylor:Taylor@cluster0.vgazn.gcp.mongodb.net/test?retryWrites=true&w=majority";
MongoClient.connect(uri, {useUnifiedTopology: true, useNewUrlParser: true}, function(err, db) {
    if (err) throw err;
    let dbo = db.db("test");
    dbo.collection("account").find({}).toArray(function(err, result)
    {
        if (err) throw err;
        console.log("Successful DB Connection!");
        data = result;
        db.close();
    });
});
// Invoke Express //
const app = express();
// Invoke Middlewares //
app.use(session({secret: SESSION_SECRET, resave: false, saveUninitialized: false}));
app.use(express.static('public'));
app.use(passport.initialize());
app.use(passport.session());
// Set App to read Pug  files for view //
app.set("view engine", "pug");

// Serve static style.css file from public folder //
app.use(express.static('public'));

// Set app to use bodyParser //
app.use(bodyParser.urlencoded({extended: true}));

// Add response for index page
app.get("/index", (req, res) => {
    res.render("index", {
        title: "Login"
    });
});
// Add response for signup page
app.get("/signup", (req, res) => {
    res.render("signup", {
        title: "Sign up"
    });
});
// Add response for home page
app.get("/home", (req, res) => {
    res.render("home", {
        title: "Home"
    });
});
// TEMPORARY 
// Add response for test page
app.get("/test", function(req, res) {
    // Validate data exists for account
    let commandList = [];
    let messageList = [];
    let dataList = [];
    if (data.length !== 0) {
        data.forEach(function(data) {
            Object.keys(data).forEach(key => {
                if (key !== "_id" && key !== "container_id") {
                    commandList.push(key);
                    messageList.push(data[key]);
                }
            });
        });
        dataList.push(commandList, messageList);
    }
    res.render("test", {
        title: "Tables",
        "userData": data,
        "commandList": commandList,
        "messageList": messageList,
        "dataList": dataList
    });
});

// Override passport profile function to get user profile from Twitch API //
OAuth2Strategy.prototype.userProfile = function(accessToken, done) {
    let options = {
        url: 'https://api.twitch.tv/helix/users',
        method: 'GET',
        headers: {
            'Client-ID': TWITCH_CLIENT_ID,
            'Accept': 'application/vnd.twitchtv.v5+json',
            'Authorization': 'Bearer ' + accessToken
        }
    };

    request(options, function (error, response, body) {
        if (response && response.statusCode == 200) {
            done(null, JSON.parse(body));
        } else {
            done(JSON.parse(body));
        }
    });
}
passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(user, done) {
    done(null, user);
});
passport.use('twitch', new OAuth2Strategy({
    authorizationURL: 'https://id.twitch.tv/oauth2/authorize',
    tokenURL: 'https://id.twitch.tv/oauth2/token',
    clientID: TWITCH_CLIENT_ID,
    clientSecret: TWITCH_SECRET,
    callbackURL: CALLBACK_URL,
    state: true
},
function(accessToken, refreshToken, profile, done) {
    profile.accessToken = accessToken;
    profile.refreshToken = refreshToken;

    /** Store user profile in db
     * User.findOrCreate(..., function(err, user) {
     * done(err, user);
     * });
     */
}
));

// ROUTE for OAuth redirect
app.get('/auth/twitch', passport.authenticate('twitch', {scope:'user_read'}));
app.get('/auth/twitch/callback', passport.authenticate('twitch', {successRedirect: '/home', failureRedirect: '/'}));

// Define a simple template to safely generate HTML with values from user's profile
let template = handlebars.compile(`
<html><head><title>Twitch Auth Sample</title></head>
<table>
    <tr><th>Access Token</th><td>{{accessToken}}</td></tr>
    <tr><th>Refresh Token</th><td>{{refreshToken}}</td></tr>
    <tr><th>Display Name</th><td>{{display_name}}</td></tr>
    <tr><th>Bio</th><td>{{bio}}</td></tr>
    <tr><th>Image</th><td>{{logo}}</td></tr>
</table></html>`);

// If user has an authenticated session, display it, otherwise display link to authenticate
app.get('/', function (req, res) {
    if(req.session && req.session.passport && req.session.passport.user) {
        res.render(template(req.session.passport.user));
    } else {
        res.render('index');
    }
});

// Add listen for localhost response //
const server = app.listen(8080, () => {
    console.log(`Site running -> PORT ${server.address().port}`);
});